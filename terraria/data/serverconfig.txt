world=/srv/terraria/data/Worlds/NuevaLogia.wld
worldname=MiPrimeraLogia
wordpath=/srv/terraria/data/Worlds

autocreate=2
difficulty=1

language=es-Es
maxplayers=4
motd="Bienvenido a la primera gran logia virtual"

password=mason
port=7777
priority=1
