#!/bin/bash

_exit_terraria() {

	echo "Exiting Terraria Server...."
	screen -XS terraria quit

}

echo "Creating Terraria Server...."
screen -dmS terraria /bin/bash -c \
	"/opt/games/terraria/TerrariaServer.bin.x86_64 -config /srv/terraria/data/serverconfig.txt"

trap '_exit_terraria' SIGTERM

while true; do :; done
