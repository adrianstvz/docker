#!/bin/bash

_generate_config() {

	echo '"cpu_threads_conf" :'
	echo '['

	nprocs=$( nproc )
	for i in $(seq 0 $(( nprocs - 1 ))); do
		echo "{\"low_power_mode\" : false, \"no_prefetch\" : true, \"asm\" : \"auto\", \"affine_to_cpu\" : $i },"
	done
	echo '],'

}

sudo apt-get update 
sudo apt-get install -y	git \
	gcc \
	cmake \
	vim \
	cmake \
	libmicrohttpd-dev \
	libssl-dev \
	build-essential \
	libhwloc-dev \
	ocl-icd-opencl-dev

mkdir monero && cd monero

git clone https://github.com/fireice-uk/xmr-stak.git -b xmr-stak-rx

mkdir xmr-stak/build -p
cd xmr-stak/build
cmake .. 
make install
sudo mv bin/xmr-stak-rx /bin/

cd ~/monero
rm -rf xmr-stak

[[ -z $WALLET ]] && WALLET="89NxQNbn7y94VWjsLAFnbnWWgVnvJPMQfgjWB3546bSBT9TyjAaxpptJMj88N75aYhf9v2bjN9uDLj4Pa1BKn1WYV3gu99K"
ADDR="pool.supportxmr.com:3333"
NAME="azure-miner-${PREFIX:-generic}"

_generate_config > cpu.txt

sudo xmr-stak-rx --noDevSupport --noTest --noAMD --noNVIDIA --currency monero --pass ${NAME} --user ${WALLET} --url ${ADDR}

